﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class AIPaddle : MonoBehaviour {

	private Rigidbody rigidbody;

	public Transform puck;
	public Transform goal;

	void Start () {
		rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;
	}

	public float speed = 20f;

	void FixedUpdate () {

		Vector3 pos = (goal.position + puck.position) / 2;

		Vector3 dir = pos - rigidbody.position;
		Vector3 vel = dir.normalized * speed;

		// check is this speed is going to overshoot the target
		float move = speed * Time.fixedDeltaTime;
		float distToTarget = dir.magnitude;

		if (move > distToTarget) {
			// scale the velocity down appropriately
			vel = vel * distToTarget / move;
		}

		rigidbody.velocity = vel;
	}

	// Update is called once per frame
	void Update () {
		
	}
}
